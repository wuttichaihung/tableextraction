import cv2
import numpy as np
import requests


class transaction:
    def __init__(self, apiKey, gray2bw_threshold=180, kernel_size=(5, 5)):
        self.kernel_size = kernel_size
        self.gray2bw_threshold = gray2bw_threshold
        self.apiKey = apiKey

    def findAllRectangle(self, image):
        img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, thresh_value = cv2.threshold(
            img_gray, self.gray2bw_threshold, 255, cv2.THRESH_BINARY_INV)

        kernel = np.ones(self.kernel_size, np.uint8)
        dilated_value = cv2.dilate(thresh_value, kernel, iterations=1)
        contours, hierarchy = cv2.findContours(
            dilated_value, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        rect_pos_list = []
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            rect_pos_list.append([x, y, w, h])
        return rect_pos_list

    def findTableRectangle(self, rect_pos_list):
        rect_area = []
        for rect_pos in rect_pos_list:
            x, y, w, h = rect_pos
            area = w*h
            rect_area.append(area)
        max_area_idx = np.argmax(rect_area)
        x, y, w, h = rect_pos_list[max_area_idx]
        return x, y, w, h

    def cropTable(self, image):
        rect_pos_list = self.findAllRectangle(image)
        x, y, w, h = self.findTableRectangle(rect_pos_list)
        img_crop_table = image[y:y+h, x:x+w]
        return img_crop_table

    def sortHeaderLR(self, header_pos):
        x_header_pos = [pos[0] for pos in header_pos]
        idx_list = np.argsort(x_header_pos)
        header_pos_sort_list = [header_pos[idx] for idx in idx_list]
        return header_pos_sort_list

    def findHeaderRectangle(self, rect_pos_list):
        header_pos = []
        for rect_pos in rect_pos_list:
            x, y, w, h = rect_pos
            if w > 50 and w < 200 and h > 50 and h < 200:
                header_pos.append([x, y, w, h])
        header_pos_sort_list = self.sortHeaderLR(header_pos)
        return header_pos_sort_list

    def cropRow(self, image):
        rect_pos_list = self.findAllRectangle(image)
        header_pos_sort_list = self.findHeaderRectangle(rect_pos_list)

        img_crop_row_list = []
        image_draw = image.copy()
        for header_pos_sort in header_pos_sort_list:
            x, y, w, h = header_pos_sort
            img_crop_row_list.append(image[y:, x:x+w])

        return img_crop_row_list

    def ocr(self, path_img):
        url = "https://api.aiforthai.in.th/ocr"
        files = {'uploadfile': open(path_img, 'rb')}
        headers = {
            'Apikey': self.apiKey,
        }

        response = requests.post(url, files=files, headers=headers)
        text = response.json()['Spellcorrection']
        return text

    def repairText(self, text):
        text_list = text.split("\n")
        repaired_text_list = []
        for text in text_list:
            if "." in text or "," in text:
                repaired_data = float(text.replace(
                    ".", "").replace(",", ""))/100
                repaired_text_list.append(repaired_data)
        return repaired_text_list


if __name__ == "__main__":
    apiKey = "api"  # change this
    tr = transaction(apiKey=apiKey)
    path_img = "lv1.jpg"
    image = cv2.imread(path_img)
    img_crop_table = tr.cropTable(image)
    img_crop_row_list = tr.cropRow(img_crop_table)

    for i, img_crop_row in enumerate(img_crop_row_list):
        path_img = "row_crop/" + str(i) + ".jpg"
        cv2.imwrite(path_img, img_crop_row)

    # 4.jpg : withdraw
    # 5.jpg : deposite
    filenames = ["4.jpg", "5.jpg"]
    for filename in filenames:
        path_img = "row_crop/" + filename
        text = tr.ocr(path_img)
        repaired_text_list = tr.repairText(text)
        print(str(filename), repaired_text_list)
